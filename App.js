import 'react-native-gesture-handler';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AppReducer from './reducers';
import MainScreen from './components/MainScreen/MainScreen';
import LevelsScreen from './components/LevelsScreen/LevelsScreen';
import GameScreen from './components/GameScreen/GameScreen';

const store = createStore(
  AppReducer,

);

const { Navigator, Screen } = createStackNavigator();

class SokobanApp extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Navigator initialRouteName={'MainScreen'}>
            <Screen name="MainScreen" component={MainScreen} />
            <Screen name="LevelsScreen" component={LevelsScreen} />
            <Screen name="GameScreen" component={GameScreen} />
          </Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}

export default SokobanApp;
