import { combineReducers } from 'redux';
import gameState from './gameState';

const AppReducer = combineReducers({
  gameState,
});

export default AppReducer;
