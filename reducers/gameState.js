import Immutable from 'immutable';
import actions from '../constants/actions';
import levels from '../constants/levels';
import {
  isMoveValid,
  updateBoard,
  undoMove,
  updatePlayerPos,
  isSolved,
  getPlayerPos
} from '../utils/gameBoard';

const initialState = {
  levelsSolved: {},
  level: null,
  board: null,
  playerPos: null,
  route: [],
  solved: false,
};

const gameState = (state = initialState, action) => {
  switch (action.type) {
    case actions.playerMove:
      const validMove = isMoveValid(state?.board, state?.playerPos, action.direction);
      if (validMove && !state?.solved) {
        const updatedBoard = updateBoard(
          state?.board,
          state?.playerPos,
          validMove.nextTile,
          action.direction,
          validMove.shouldPush);
        const levelSolved = isSolved(updatedBoard);
        const newState = {
          ...state,
          board: updatedBoard,
          playerPos: updatePlayerPos(state?.playerPos, action.direction),
          route: state?.route.push({ direction: action.direction, shouldPush: validMove.shouldPush }),
          solved: levelSolved
        }

        levelSolved
          ? newState.levelsSolved = state?.level
          : newState;
        return newState;
      };

      return state;

    case actions.playerMoveUndo:
      const latestMove = state.route.last();
      return latestMove
        ? state.withMutations(state =>
          state
            .set('board', undoMove(state.board, state.playerPos, latestMove))
            .set('playerPos', updatePlayerPos(state.playerPos, latestMove.direction, -1))
            .set('route', state.route.pop())
        )
        : state;
    case actions.levelLoad:
      const level = action.level;
      const levelArray = levels.filter(l => l.level === level);

      let loadedBoard = levelArray[0]?.gameMap;

      if (loadedBoard === null || loadedBoard === undefined) {
        loadedBoard = [
          ['#', '#', '#', '#', '#', '#', '#'],
          ['#', ' ', ' ', ' ', ' ', '.', '#'],
          ['#', ' ', '#', ' ', '#', ' ', '#'],
          ['#', '$', ' ', '@', ' ', '$', '#'],
          ['#', ' ', '#', ' ', '#', ' ', '#'],
          ['#', '.', ' ', ' ', ' ', ' ', '#'],
          ['#', '#', '#', '#', '#', '#', '#'],
        ];
      }

      return {
        ...state,
        level: action.level,
        board: loadedBoard,
        playerPos: getPlayerPos(loadedBoard),
        solved: false,
        route: new Immutable.List()
      };

    default:
      return state;
  }
};

export default gameState;
