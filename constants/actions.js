module.exports = {
  playerMove: 'playerMove',
  playerMoveUndo: 'playerMoveUndo',
  levelLoad: 'levelLoad',
  navigate: 'navigate',
  goBack: 'goBack'
};