import Immutable from 'immutable';
import directions from '../constants/directions';
import boardElements from '../constants/boardElements';

function updateBoard(board, playerPos, nextTile, direction, shouldPush) {
  const currentTile = board[playerPos.row, playerPos.col];
  const newPlayerPos = updatePlayerPos(playerPos, direction);
  const newBoxPos = updatePlayerPos(playerPos, direction, 2);

  return board.withMutations(b =>
    shouldPush
      ? b.setIn(
        [playerPos.row, playerPos.col],
        currentTile === boardElements.playerOnGoal ? boardElements.goal : boardElements.floor)

        .setIn(
          [newPlayerPos.row, newPlayerPos.col],
          nextTile === boardElements.boxOnGoal || nextTile === boardElements.goal
            ? boardElements.playerOnGoal
            : boardElements.player)

        .setIn(
          [newBoxPos.row, newBoxPos.col],
          board[newBoxPos.row, newBoxPos.col] === boardElements.goal
            ? boardElements.boxOnGoal
            : boardElements.box)

      : b.setIn(
        [playerPos.row, playerPos.col],
        currentTile === boardElements.playerOnGoal ? boardElements.goal : boardElements.floor)

        .setIn(
          [newPlayerPos.row, newPlayerPos.col],
          nextTile === boardElements.goal
            ? boardElements.playerOnGoal
            : boardElements.player)
  );
}

function isSolved(board) {
  return !board.some(row => row.includes(boardElements.box));
}

function getNextTile(board, playerPos, direction, isBox = false) {
  const distance = isBox ? 2 : 1;
  switch (direction) {
    case directions.up: return board[playerPos.row - distance, playerPos.col];
    case directions.down: return board[playerPos.row + distance, playerPos.col];
    case directions.right: return board[playerPos.row, playerPos.col + distance];
    case directions.left: return board[playerPos.row, playerPos.col - distance];
  }
}

function isMoveValid(board, playerPos, direction) {
  const nextTile = getNextTile(board, playerPos, direction);
  if (nextTile === boardElements.floor || nextTile === boardElements.goal) {
    return {
      nextTile,
      shouldPush: false,
    };
  }
  if (nextTile === boardElements.box || nextTile === boardElements.boxOnGoal) {
    const nextBoxTile = getNextTile(board, playerPos, direction, true);
    if (nextBoxTile === boardElements.floor || nextBoxTile === boardElements.goal) {
      return {
        nextTile,
        shouldPush: true,
      };
    }
  }
  return false;
}

function updatePlayerPos(playerPos, direction, distance = 1) {
  switch (direction) {
    case directions.up: return playerPos.row = playerPos.row - distance;
    case directions.down: return playerPos.row = playerPos.row + distance;
    case directions.right: return playerPos.col = playerPos.col + distance;
    case directions.left: return playerPos.col = playerPos.col - distance;
  }
}

function getRelativePos(position, direction, distance) {
  switch (direction) {
    case directions.up: return position.row = position.row - distance;
    case directions.down: return position.row = position.row + distance;
    case directions.left: return position.col = position.col - distance;
    case directions.right: return position.col = position.col + distance;
    default: return position;
  }
}

function getPlayerPos(board) {
  let col = -1;
  let row = board.findIndex((row) => {
    let indexInRow = row.findIndex(element =>
      element === boardElements.player || element === boardElements.playerOnGoal);
    if (indexInRow > -1) {
      col = indexInRow;
      return true;
    }
    return false;
  });
  return Immutable.fromJS({
    col,
    row,
  });
}

function undoMove(board, playerPos, latestMove) {
  const boxPosition = getRelativePos(playerPos, latestMove.direction, 1);
  const boxTile = board[boxPosition.row, boxPosition.col];
  const playerTile = board[playerPos.row, playerPos.col];
  const previousPosition = getRelativePos(playerPos, latestMove.direction, -1);
  const previousTile = board[previousPosition.row, previousPosition.col];
  let newBoxTile;
  switch (boxTile) {
    case boardElements.wall:
      newBoxTile = boardElements.wall;
      break;
    case boardElements.box:
      newBoxTile = latestMove.shouldPush ? boardElements.floor : boardElements.box;
      break;
    case boardElements.floor:
      newBoxTile = boardElements.floor;
      break;
    case boardElements.boxOnGoal:
      newBoxTile = latestMove.shouldPush ? boardElements.goal : boardElements.boxOnGoal;
      break;
    case boardElements.goal:
      newBoxTile = boardElements.goal;
      break;
    default:
      newBoxTile = boxTile;
  }

  let newPlayerTile;
  if (playerTile === boardElements.playerOnGoal) {
    if (latestMove.shouldPush
      && (boxTile === boardElements.box || boxTile === boardElements.boxOnGoal)) {
      newPlayerTile = boardElements.boxOnGoal;
    } else {
      newPlayerTile = boardElements.goal;
    }
  } else {
    if (latestMove.shouldPush
      && (boxTile === boardElements.box || boxTile === boardElements.boxOnGoal)) {
      newPlayerTile = boardElements.box;
    } else {
      newPlayerTile = boardElements.floor;
    }
  }

  return board.withMutations(b =>
    b
      .setIn([boxPosition.row, boxPosition.col], newBoxTile)
      .setIn([playerPos.row, playerPos.col], newPlayerTile)
      .setIn([previousPosition.row, previousPosition.col],
        previousTile === boardElements.goal
          ? boardElements.playerOnGoal
          : boardElements.player)
  );
}

module.exports = {
  updateBoard,
  updatePlayerPos,
  isSolved,
  isMoveValid,
  getPlayerPos,
  undoMove,
};
